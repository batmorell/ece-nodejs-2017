const http = require('http');
const user = require('./user.js');

http.createServer(function(req, res) {

  if(req.url === '/save') {
    user.save('testUser', 'testPassword', function() {
      res.writeHead(201, { 'Content-Type': 'text/plain' });
      res.end('User is saved\n');
    });
  } else if(req.url === '/get') {
    user.get('testUser', function() {
      res.writeHead(200, { 'Content-Type': 'text/plain' });
      res.end('User save\n');
    });
  } else if(req.url === '/') {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('Hello World\n');
  } else {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Route not found\n');
  }

}).listen(8888, '127.0.0.1')
