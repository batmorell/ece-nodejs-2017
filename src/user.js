module.exports = {
  save: function(name, password, callback) {
    if (callback == null) {
      password(new Error("missing parameters"))
    } else {
      console.log(`Saving ${name} ${password}`);
      callback()
    }
  },
  get: function(name, callback) {
    console.log(`Getting ${name}`);
    callback();
  },
}
